<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    private $MODULE_PATH = "admin/permissions";

    public function __construct() {
        parent::__construct();

        $this->load->library('pagination');
        $this->load->library('Up_pagination');
        $this->load->model('Mdl_permissions');
        $this->load->model('groups/Mdl_groups');
        $this->load->model('modules/Mdl_moduleslist');
        $this->load->module('admin_login/admin_login');
        $this->admin_login->check_session_and_permission('permissions'); //module name is permissions here	
    }

    function index() {
        $main_table_params = 'id,name';
        if ($this->input->post('parameters')) {
            $params = json_decode($this->input->post('parameters'));
        } else {
            $params = '';
            
        }
        $count = $this->Mdl_groups->count($params);
        if ($this->input->post('order_by')) {
            $order_by = $this->input->post('order_by');
        } else {
            $order_by = 'id';
        }

        
        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
        $config['per_page'] =5;
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
        $data['query'] = $this->Mdl_groups->get_where_dynamic('', $main_table_params, 'id', ($page - 1) * $config['per_page'], $config['per_page'], $params);
        $data['columns'] = array('name', 'set_permission');
        $data['page'] = $page;
        $data['total'] = $count;
        $data['total_page'] = ceil($count / $config['per_page']);
        $data['per_page'] = $config['per_page'];
        $data['theUrl'] = $module_url;
        $data['page_links'] = $this->pagination->create_links();
        if ($this->uri->segment(3) == '' && ($params == '')) {
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            $this->load->view('admin/new_table', $data);
        }
    }

//    function get_groups($order_by) {
//        $this->load->model('groups/mdl_groups');
//        $query = $this->mdl_groups->get($order_by);
//        return $query;
//    }

    function group() {
        $group_id = $this->uri->segment(4);
        if (!empty($group_id)) {
            $data['permissions'] = $this->Mdl_permissions->unserialize_role_array($group_id);
            $data['ids'] = $this->Mdl_moduleslist->get_module_ids();
            $data['query'] = $this->Mdl_moduleslist->get_where_dynamic('','','id');
            $data['view_file'] = "admin/group";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            $this->session->set_flashdata('error', 'The group id provided was not valid.');
            redirect('admin/permissions');
        }
    }

//    function get_module_ids() {
//        $query = $this->Mdl_moduleslist->get_module_ids();
//        return $query;
//    }
//
//    function get_modules($order_by) {
//        $query = $this->Mdl_moduleslist->get($order_by);
//        return $query;
//    }

    function get_data_from_post() {
        $query = $this->Mdl_moduleslist->get_module_ids();
        /* $query brings all id of modules */
        foreach ($query as $row) {
            $data[$row] = $this->input->post($row, TRUE); /* this gives 'on' */
            $v = $this->input->post('v' . $row, TRUE);
            $e = $this->input->post('e' . $row, TRUE);
            $d = $this->input->post('d' . $row, TRUE);
            $a = $this->input->post('a' . $row, TRUE);
            if ($data[$row] != 'on') {
                unset($data[$row]);
            } else {
                $data[$row] = $row;
                if ($v == 'on') {
                    //unset($data[$row]);
                    $data[$row] = $data[$row] . 'v' . $row;
                }
                if ($e == 'on') {
                    //unset($data[$row]);
                    $data[$row] = $data[$row] . 'e' . $row;
                }
                if ($d == 'on') {
                    //unset($data[$row]);
                    $data[$row] = $data[$row] . 'd' . $row;
                }
                if ($a == 'on') {
                    //unset($data[$row]);
                    $data[$row] = $data[$row] . 'a' . $row;
                }
            }
        }
        $data['group_id'] = $this->input->post('group_id', TRUE);
        return $data;
    }

    function submit() {
        $data = $this->get_data_from_post();
        $group_id = $data['group_id'];
        unset($data['group_id']);
        $permission_status = $this->Mdl_permissions->check_permission_existence($group_id); //$permission_status only returns either TRUE or FALSE 
        //by checking if permission hase been set before
        if ($permission_status == TRUE) {
            $this->Mdl_permissions->_update_permissions($group_id, $data);
        } else {
            $this->Mdl_permissions->_insert_permissions($group_id, $data);
        }
        redirect('admin/permissions');
    }

//    function check_permission_existence($group_id) {
//        $query = $this->mdl_permissions->check_permission_existence($group_id);
//        return $query;
//    }
//
//    function _update($group_id, $data) {
//        $this->load->model('mdl_permissions');
//        $this->mdl_permissions->_update($group_id, $data);
//    }
//
//    function _insert($group_id, $data) {
//        $this->load->model('mdl_permissions');
//        $this->mdl_permissions->_insert($group_id, $data);
//    }
//    function unserialize_role_array($group_id) {
//        $this->load->model('mdl_permissions');
//        $array = $this->mdl_permissions->unserialize_role_array($group_id);
//        return $array;
//    }
}
