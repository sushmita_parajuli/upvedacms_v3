-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 29, 2016 at 03:50 PM
-- Server version: 5.6.28-0ubuntu0.15.10.1
-- PHP Version: 5.6.19-1+deb.sury.org~wily+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `upvedacms_v3`
--

-- --------------------------------------------------------

--
-- Table structure for table `up_settings`
--

CREATE TABLE IF NOT EXISTS `up_settings` (
  `id` int(11) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `meta_topic` varchar(255) NOT NULL,
  `meta_data` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `date_format` varchar(255) NOT NULL,
  `frontend_enabled` enum('open','closed') NOT NULL DEFAULT 'open',
  `unavailable_message` text NOT NULL,
  `favicon` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `per_page` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `up_settings`
--

INSERT INTO `up_settings` (`id`, `site_name`, `meta_topic`, `meta_data`, `contact_email`, `date_format`, `frontend_enabled`, `unavailable_message`, `favicon`, `logo`, `per_page`) VALUES
(1, 'UpvedaCMS', 'Upveda Technology Pvt. Ltd.', 'A company that provides web solutions.', 'sachit_karki@hotmail.com', 'F j, Y', 'open', 'Sorry, this website is currently unavailable. Please try again later. Thank You!', 'favicon.ico', 'logo.png', 10);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
