<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mdl_task extends CI_Model{
    function _construct()
    { 
        //call the model constructor 
        parent::_construct();
    }
    
    function get($order_by){
        
        $this->db->order_by($order_by);
        $query=$this->db->get('up_task');
        return $query;
    }
}