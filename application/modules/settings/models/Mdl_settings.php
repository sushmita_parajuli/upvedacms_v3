<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_settings extends Mdl_crud {

    protected $_table = "up_settings";
    protected $_primary_key = 'id';

    function __construct() {
        parent::__construct();
    }

    function get_settings() {
        $table = $this->_table;
        $query = $this->db->get($table);
        return $query;
    }

    function get_where($name) {
        $table = $this->_table;
        $this->db->select($name);
        $this->db->where($this->_primary_key, 1);
        $query = $this->db->get($table);
        return $query;
    }

}
