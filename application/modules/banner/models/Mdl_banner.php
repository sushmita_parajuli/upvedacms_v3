<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_banner extends Mdl_crud {

    protected $_table = "up_banner";
    protected $_primary_key = 'id';

    function __construct() {
        parent::__construct();
    }

    function get_id() {
        $nextId = $row['Auto_increment'];
        $result = $this->_custom_query("SHOW TABLE STATUS LIKE 'up_banner'");
        $row = $result->result();
        $nextId = $row[0]->Auto_increment;

        return $nextId;
    }

    function get_banner() {
        $table = $this->_table;
        $this->db->where('status', 'live');
        $query = $this->db->get($table);
        return $query;
    }

    function get_banner_dropdown() {
        $this->db->select('id, title');
        $this->db->order_by('title');
        $dropdowns = $this->db->get('banner')->result();
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->title;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

}
