<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_sample_attachment extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_table() {
        $table = "up_sample_attachment";
        return $table;
    }

    function _delete($id) {
        $table = $this->get_table();
        $this->db->where('id', $id);
        $this->db->delete($table);
    }

    function get($order_by) {
        $table = $this->get_table();
        $this->db->order_by($order_by, 'ASC');
        $query = $this->db->get($table);
        return $query;
    }

    function get_where($id) {
        $table = $this->get_table();
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        return $query;
    }

    function get_details($slug) {
        $table = 'up_sample_attachment';
        $this->db->where('slug', $slug);
        $query = $this->db->get($table);
        return $query;
    }

    function _insert($data) {
        $next_id = $this->get_id();

        $table = $this->get_table();
        $this->db->insert($table, $data);


//	$insert_null_permission_array = array('group_id' => $next_id, 'roles' => 'a:0:{}');
//	$permission_table = 'up_permissions';	
//	$this->db->insert($permission_table, $insert_null_permission_array);
    }

    function get_id() {
        $table = $this->get_table();
        $this->db->order_by('id','DESC');
        $query = $this->db->get($table)->result_array();
//        var_dump($query[0]['id']+1); die;
        return $query[0]['id']+1;
//        $result = $this->db->query("SHOW TABLE STATUS LIKE 'up_sample_attachment'");
//        $row = mysql_fetch_array($result);
//        die($row);
//        $nextId = $row['Auto_increment'];
//        return $nextId;
    }

    function _update($id, $data) {
        $table = $this->get_table();
        $this->db->where('id', $id);
        $this->db->update($table, $data);
    }

    function get_modules_dropdown() {
        $this->db->select('id, name');
        $this->db->order_by('name');
        $dropdowns = $this->db->get('up_modules')->result();
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->name;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

    function get_groups_dropdown() {
        $this->db->select('id, title');
        //$this->db->where('id > 1');
        $this->db->order_by('id', 'AESC');
        $dropdowns = $this->db->get('up_sample_attachment')->result();
        foreach ($dropdowns as $dropdown) {
            //$dropdownlist[0] = '-- Select Panchakarma --';    
            $dropdownlist[$dropdown->id] = $dropdown->title;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

    function get_sample_attachment($order_by) {
        $table = $this->get_table();
        $this->db->order_by('id', 'ASC');
        $this->db->where('status', 'live');
        $query = $this->db->get($table)->result_array();
        return $query;
    }

    public function upload_image($fileName) {
        if ($fileName != '') {
            $fileName1 = explode(',', $fileName);
            foreach ($fileName1 as $file) {
                $file_data = array(
                    'attachment' => $file,
//                    'datetime' => date('Y-m-d h:i:s')
                );
//                print_r($file_data);die();
                $this->db->insert("up_sample_attachment", $file_data);
            }
        }
    }

}
