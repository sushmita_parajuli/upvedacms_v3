<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    private $MODULE_PATH = "admin/modules";
    private $group_id;
    private $model_name = 'Mdl_moduleslist';
    private $MODULE ='modules';

    public function __construct() {
        parent::__construct();
        $this->group_id = $this->session->userdata('group_id');
        $this->load->model('permissions/Mdl_permissions');
        $this->load->library('pagination');
        $this->load->library('Up_pagination');
        $this->load->library('Common_functions');
        $this->load->model('Mdl_moduleslist');
        $this->load->model('settings/Mdl_settings');
        $this->load->module('admin_login/admin_login');
        $this->admin_login->check_session_and_permission('modules'); //module name is modules here	
    }

    function index() {
        //table select parameters
        $main_table_params = 'id,title,slug';
        //search parameters
        if ($this->input->post('parameters')) {
            $params = json_decode($this->input->post('parameters'));
            $count = $this->Mdl_moduleslist->count($params);
        } else {
            $params = '';
        }
         $count = $this->Mdl_moduleslist->count($params);

        if ($this->input->post('order_by')) {
            $order_by = $this->input->post('order_by');
        } else {
            $order_by = 'id';
        }

        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
        $entries = $this->common_functions->get_data_from_db('1', 'per_page', 'Mdl_settings');
        $config['per_page'] = $entries['per_page'];
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
//        $data['query'] = $this->Mdl_moduleslist->get_all_for_pagination('id', ($page - 1) * $config['per_page'], $config['per_page']);
        $data['query'] = $this->Mdl_moduleslist->get_where_dynamic('', $main_table_params, $order_by, ($page - 1) * $config['per_page'], $config['per_page'], $params);
        $data['permission'] = $this->common_functions->check_permission($this->group_id,$this->MODULE);
        $data['page'] = $page;
        $data['total'] = $count;
        $data['total_page'] = ceil($count / $config['per_page']);
        $data['per_page'] = $config['per_page'];
        $data['theUrl'] = $module_url;
        $data['page_links'] = $this->pagination->create_links();
        $data['column'] = 'modules';
        $data['title'] = 'title';
        $data['slug'] = 'slug';
        if ($this->uri->segment(3) == '' && ($params == '')) {
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            $this->load->view('admin/new_table', $data);
        }
    }

    function get_data_from_post() {
        $data['title'] = $this->input->post('title', TRUE);
        $data['slug'] = strtolower(url_title($data['title']));

        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {
            $data['upd_date'] = date("Y-m-d");
        } else {
            $data['ent_date'] = date("Y-m-d");
            $data['upd_date'] = NULL;
        }
        return $data;
    }

    function create() {
        $update_id = base64_decode($this->uri->segment(4));
        $submit = $this->input->post('submit', TRUE);

        if ($submit == "Submit") {
//person has submitted the form
            $data = $this->get_data_from_post();
        } else {
            if (is_numeric($update_id)) {
                $select = 'title';
                $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
            }
        }

        if (!isset($data)) {
            $data = $this->get_data_from_post();
        }

        $data['update_id'] = $update_id;

        $data['view_file'] = "admin/form";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function delete() {
        $delete_id = base64_decode($this->uri->segment(4));

        if (!isset($delete_id) || !is_numeric($delete_id)) {
            unset($delete_id);
            redirect('admin/modules');
        } else {
            $this->Mdl_moduleslist->_delete($delete_id);
            redirect('admin/modules');
        }
    }

    function submit() {

        $this->load->library('form_validation');
        /* setting validation rule */
        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {
            $this->form_validation->set_rules('title', 'Title', 'required|xss_clean'); //we don't want unique_validation error while editing
        } else {
            $this->form_validation->set_rules('title', 'Title', 'required|xss_clean|is_unique[up_modules.title]|is_unique[up_pages.title]'); //unique_validation check while creating new
        }
        /* end of validation rule */

        if ($this->form_validation->run($this) == FALSE) {
            $this->create();
        } else {
            $data = $this->get_data_from_post();
            $update_id = $this->input->post('update_id', TRUE);
            if (is_numeric($update_id)) {
                $permission = $this->common_functions->check_permission($this->group_id);
                if (isset($permission['edit'])) {
                    $this->Mdl_moduleslist->_update($update_id, $data);
                    $this->session->set_flashdata('operation', 'Updated Successfully!!!');
                }
            } else {
                $permission = $this->common_functions->check_permission($this->group_id);
                print_r($permission);
                if (isset($permission['add'])) {
                    $this->Mdl_moduleslist->_insert($data);
                    $this->session->set_flashdata('operation', 'Inserted Successfully!!!');
                }
            }

            redirect('admin/modules');
        }
    }

}
