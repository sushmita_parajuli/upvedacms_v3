<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Template extends MX_Controller {

    function __construct() {
        parent::__construct();
        if($this->session->userdata('language') == "eng")
		{
			$this->lang->load('english', 'english');
		}
		else if($this->session->userdata('language')=="nep")
		{
			$this->lang->load('nepali', 'nepali');
		}
                else {
                $this->lang->load('english', 'english');
                }
    }

    function front($data) {

         $language_id=$data['language_id'];
//         print_r($language_id);die;
          if(!$this->session->userdata('language')){
                   $data['language']= $selected_language = 'eng';
                }
                else{
                    $data['language']=$selected_language=$this->session->userdata['language'];
                }
                $selected_language_id = $this->get_selected_language_id($selected_language);
                $data['language_list']=$this->get_language_list();
        $nav = $this->session->userdata('navtype');
        $page = $this->get_page_live();
        foreach ($page as $row) {
            $page_name [] = $row['slug'];
        }
        $module_name = $this->uri->segment(1);
        $slug = $this->uri->segment(3);
        if (!empty($slug)) {
            $data['seo'] = $this->get_metadata_search_keys($module_name, $slug);
        } else {
            if ($module_name != '' && in_array($module_name, $page_name) == '' && $module_name != 'contactus') {
                $data['seo'] = $this->get_metadata_search_module($module_name);
            }
        }
        $data['site_settings'] = $this->get_site_settings();
        $data['banner'] = $this->get_banner();
        $data['headernav'] = $this->get_header('1',$selected_language_id); //this is id number of header, and since header can't be edited or deleted, we are using this id number to be precise
//		var_dump($data['headernav']);die;
        $data['footernav'] = $this->get_footer('2',$language_id); //similar as above but 2 is for footer nav
        $this->load->view('front', $data);
    }

    function get_site_settings() {
        $this->load->model('settings/mdl_settings');
        $query = $this->mdl_settings->get_settings();
        $result = $query->result_array();
        return $result[0];
    }

    function get_navigation_from_navigation_name($navigation_name) {
        $this->load->model('navigation/mdl_navigation');
        $query = $this->mdl_navigation->get_navigation_from_navigation_name($navigation_name);
        $result = $this->add_href($query->result_array());
        return $result;
    }

    function errorpage() {
        $this->load->view('404');
    }

    function userlogin($data) {
        $this->load->view('userlogin', $data);
    }

    function get_banner() {
        $this->load->model('banner/mdl_banner');
        $query = $this->mdl_banner->get_banner();
        return $query->result_array();
    }

    function get_parent($group_id,$language_id) {
        $this->load->model('navigation/Mdl_navigation');
        $query = $this->Mdl_navigation->get_parentnav_for_frontend($group_id,$language_id);
        
        $result = $this->add_href($query);
//        var_dump($result);die;
        return $result;
    }

    function get_child($group_id, $parent_id, $language_id) {
        $this->load->model('navigation/Mdl_navigation');
        $query = $this->Mdl_navigation->get_childnav_for_frontend($group_id, $parent_id,$language_id);
        $result = $this->add_href($query);
        return $result;
    }

    function get_header($group_id,$language_id) {
        $data['parentnav'] = $this->get_parent($group_id,$language_id); //
//                var_dump($data['parentnav']);die;
        if ($data['parentnav'] == NULL) {
            return NULL;
        }
        $i = 0;
        foreach ($data['parentnav'] as $nav) {
            $children = $this->get_child($group_id, $nav['id'],$language_id);
            if ($children != NULL) {
                $nav['children'] = $children;
            }
            $navigation[$i] = $nav;
            $i++;
        }
        return $navigation;
    }

    function get_footer($group_id,$language_id) {
        
        $this->load->model('mdl_template');
        $query = $this->mdl_template->get_footernav($group_id);
        $result = $this->add_href($query->result_array());
        return $result;
    }

    function add_href($result) {
        $count = count($result);
        for ($i = 0; $i < $count; $i++) {
            if ($result[$i]['navtype'] == 'Module') {
                $result[$i]['href'] = $this->get_name_from_module($result[$i]['module_id']);
                $result[$i]['target'] = "_self";
            } elseif ($result[$i]['navtype'] == 'Page') {
                $result[$i]['href'] = $this->get_name_from_page($result[$i]['page_id']);
                $result[$i]['target'] = "_self";
            } elseif ($result[$i]['navtype'] == 'URI') {
                $result[$i]['href'] = $result[$i]['site_uri'];
                $result[$i]['target'] = "_self";
            } else {
                $result[$i]['href'] = $result[$i]['link_url'];
                $result[$i]['target'] = "_blank";
            }
        }
        return $result;
    }

    function get_name_from_module($module_id) {
        $this->load->model('mdl_template');
        $query = $this->mdl_template->get_name_from_module($module_id);
        if (isset($query['0'])) {
            return $query['0']['slug'];
        }
    }

    function get_name_from_page($page_id) {
        $this->load->model('mdl_template');
        $query = $this->mdl_template->get_name_from_page($page_id);
        if (isset($query['0'])) {
            return $query['0']['slug'];
        }
    }

    function get_page_live() {
        $this->load->model('pages/mdl_pages');
        $query = $this->mdl_pages->get_page_live();
        return $query->result_array();
    }

    function get_metadata_search_keys($module_name, $slug) {
        $this->load->model('mdl_template');
        $query = $this->mdl_template->get_metadata_search_keys($module_name, $slug);
        return $query->result_array();
    }

    function get_metadata_search_module($module_name) {
        $this->load->model('mdl_template');
        $query = $this->mdl_template->get_metadata_search_module($module_name);
        return $query;
    }
     function get_language_list(){
        $this->load->model('language/mdl_language');
	$query = $this->mdl_language->get('id');	
	return $query->result();   
        }
         
        function get_selected_language_id($selected_language){
        $this->load->model('language/mdl_language');
        $language_id=  $this->mdl_language->get_language_id($selected_language);
        foreach($language_id -> result() as $id){
        $selected_language_id = $id->id;}
        return  $selected_language_id;
        }
}
