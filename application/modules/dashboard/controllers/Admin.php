<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

   public function __construct()
   {
		parent::__construct();
		//always check if session userdata value "logged_in" is not true
		if(!$this->session->userdata("logged_in"))/*every logged_in user get privilage to access dashboard*/
		{
			 redirect('admin');
		}

   }
	
	function index()//this is admin/dashboard(check config/routes.php to be clear
	{		
		$data['view_file'] = "admin/home";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);	
	}

}