<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_admin_login extends Mdl_crud {

    protected $_table = "admin_login";
    protected $_primary_key = 'id';

    function __construct() {
        parent::__construct();
    }

    function check_login($username, $password) {
        $hashed_pword = $this->enc_hash_pwd($password);
        $query_str = "SELECT id, group_id FROM up_users WHERE ( username = '$username' or email = '$username') and password = '$hashed_pword'";
        try {
            $result = $this->db->query($query_str, array($username, $hashed_pword));
            throw new Exception("Error 0: Invalid Username or Password!!!!");
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        if ($result->num_rows() == 1) {
            return $result->row(0);
        } else {
            return false;
        }
    }

    function enc_hash_pwd($pword) { //this function is to generate a hashed pwd
        $hashed_pwd = hash('sha512', $pword . config_item('encryption_key'));
        return $hashed_pwd;
    }

}
