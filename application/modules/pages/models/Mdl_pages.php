<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_pages extends Mdl_crud {

    protected $_table = "up_pages";
    protected $_primary_key = 'id';

    function __construct() {
        parent::__construct();
    }

    function get_pages_dropdown() {
        $this->db->select('id, title');
        $this->db->order_by('title');
        $dropdowns = $this->db->get('up_pages')->result();
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->title;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

//    function get($order_by){
//    $table = $this->_table;
//    $this->db->order_by($order_by);
//    $query=$this->db->get($table);
//    return $query;
//    }
//
//    function get_with_limit($limit, $offset, $order_by) {
//    $table = $this->_table;
//    $this->db->limit($limit, $offset);
//    $this->db->order_by($order_by);
//    $query=$this->db->get($table);
//    return $query;
//    }
//
//    function get_where($id){
//    $table = $this->_table;
//    $this->db->where('id', $id);
//    $query=$this->db->get($table);
//    return $query;
//    }

    function get_pages_if_live($col, $value,$language_id) {
        $table = $this->_table;
        $this->db->where($col, $value);
        $this->db->where('status', 'live');
        $query = $this->db->get($table);
        return $query;
    }
//
//    function get_where_dynamic($col, $value) {
//    $table = $this->_table;
//    $this->db->where($col, $value);
//    $query=$this->db->get($table);
//    return $query;
//    }
//
//    function _insert($data){
//    $table = $this->_table;
//    $this->db->insert($table, $data);
//    }
//
//    function _update($id, $data){
//    $table = $this->_table;
//    $this->db->where('id', $id);
//    $this->db->update($table, $data);
//    }
//
//    function _delete($id){
//    $table = $this->_table;
//    $this->db->where('id', $id);
//    $this->db->delete($table);
//    }
//
//    function count_where($column, $value) {
//    $table = $this->_table;
//    $this->db->where($column, $value);
//    $query=$this->db->get($table);
//    $num_rows = $query->num_rows();
//    return $num_rows;
//    }
//
//    function count_all() {
//    $table = $this->_table;
//    $query=$this->db->get($table);
//    $num_rows = $query->num_rows();
//    return $num_rows;
//    }
//
//    function get_max() {
//    $table = $this->_table;
//    $this->db->select_max('id');
//    $query = $this->db->get($table);
//    $row=$query->row();
//    $id=$row->id;
//    return $id;
//    }
//
//    function _custom_query($mysql_query) {
//    $query = $this->db->query($mysql_query);
//    return $query;
//    }
    function get_id() {
      
        $result = $this->db->query("SHOW TABLE STATUS LIKE 'up_pages'")->result();
        
        $nextId = $result[0]->Auto_increment;
        return $nextId;
    }

    function get_what_you_get($slug) {
        $table = $this->_table;
        $this->db->where('slug', $slug);
        $query = $this->db->get('up_pages');
        return $query->result_array();
    }

    function get_how_it_work($slug) {
        $table = $this->_table;
        $this->db->where('slug', $slug);
        $query = $this->db->get('up_pages');
        return $query->result_array();
    }

//    function get_page_live() {
//        $table = $this->_table;
//        $this->db->where('status', 'live');
//        $query = $this->db->get($table);
//        return $query;
//    }
     function update_id_for_module_edit($lang_id,$slug){
        $table = $this->_table;
	$this->db->where('language_id', $lang_id);
        $this->db->where('slug', $slug);
        $this->db->order_by('id','asc');
        $query=$this->db->get($table);
//      var_dump($query->result());die;
	return $query;  
        }
}
