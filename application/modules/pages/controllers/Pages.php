<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Mdl_pages');
    }

   function index(){
//       die('here');
     
                 //   $selected_language =  'eng';
                
    // $this->session->unset_userdata('language');
        //      $this->session->set_userdata('language', 'eng');
//echo 'this is page';die;
		 $first_bit = $this->uri->segment(1);
		 $second_bit = $this->uri->segment(2);
                 if($second_bit==""){
                     if($first_bit==""){
                         $first_bit="home";
                     }
                 }
		// $third_bit = $this->uri->segment(3);
                   //   echo $third_bit; die;
			 // echo 'this is language session';die;
            $language_mod= $this->input->post('slug');
            if($language_mod == 'eng'){
                $this->session->unset_userdata('language');
              $this->session->set_userdata('language', 'eng');
            }
            else if($language_mod == 'nep'){
                $this->session->unset_userdata('language');
                $this->session->set_userdata('language', 'nep');
             }
//              else if($language_mod == 'chn'){
//                $this->session->unset_userdata('language');
//                $this->session->set_userdata('language', 'eng');
//             }
//              else if($language_mod == 'tib'){
//                $this->session->unset_userdata('language');
//                $this->session->set_userdata('language', 'eng');
//             }
                         $language=$this->session->userdata('language');
                        // echo $language; die;
                         $language_id=$this->get_language_id($language);
                         if(!isset($language_id)){
                                $language_id = 3;
                         }
//                         $second_bit = $this->uri->segment(2);  
                        
			$query = $this->get_pages_if_live('slug', $first_bit,$language_id);
			foreach($query->result() as $row){
				$data['title'] = $row->title;					
				$data['slug'] = strtolower(url_title($data['title']));
				$data['description'] = $row->description;
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
                                $data['language_id'] = $row->language_id;
                                $data['meta_description'] = $row->meta_description;
                                $data['search_keys'] = $row->search_keys;
				
			}
//			var_dump($query->num_rows()); die;
			if($query->num_rows() == 0)
			{
				$this->load->module('template');
				$this->template->errorpage();
			}
			else
			{
				$this->load->module('template');
				$this->template->front($data);
			}	
		
	}

    function get_pages_if_live($col, $value,$language_id) {
        $query = $this->Mdl_pages->get_pages_if_live($col, $value,'language_id');
        return $query;
    }

    function get_where_custom($col, $value) {
        $query = $this->Mdl_pages->get_where_custom($col, $value);
        return $query;
    }

    function get_how_we_have_dream() {
        $query = $this->Mdl_pages->get_how_we_have_dream();
        return $query;
    }

    function get_short_terms_goals() {
        $query = $this->Mdl_pages->get_short_terms_goals();
        return $query;
    }

    function get_long_terms_goals() {
        $query = $this->Mdl_pages->get_long_terms_goals();
        return $query;
    }

    function get_children() {
        $query = $this->Mdl_pages->get_children();
        return $query;
    }
     function get_language_id($language){
        $this->load->model('language/Mdl_language');
	$query = $this->Mdl_language->get_language_id($language);
        foreach($query->result()as $row ){
        $language_id=$row->id;
         return $language_id;
        }

}
}

