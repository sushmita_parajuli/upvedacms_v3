<script>
$(window).load(function() {
	
    if (document.getElementById('moduleCheck').checked) {
        document.getElementById('ifModule').style.display = 'block';
		document.getElementById('ifPage').style.display = 'none';		
		document.getElementById('ifUri').style.display = 'none';
		document.getElementById('ifUrl').style.display = 'none';
    }
	else if (document.getElementById('pageCheck').checked) {
        document.getElementById('ifPage').style.display = 'block';
        document.getElementById('ifModule').style.display = 'none';		
		document.getElementById('ifUri').style.display = 'none';
		document.getElementById('ifUrl').style.display = 'none';
    }
    else if (document.getElementById('uriCheck').checked) {			
		document.getElementById('ifUri').style.display = 'block';
        document.getElementById('ifModule').style.display = 'none';
        document.getElementById('ifPage').style.display = 'none';	       
		document.getElementById('ifUrl').style.display = 'none';	
		}
    else if(document.getElementById('urlCheck').checked) {					
		document.getElementById('ifUrl').style.display = 'block';
        document.getElementById('ifModule').style.display = 'none';			
        document.getElementById('ifPage').style.display = 'none';			
		document.getElementById('ifUri').style.display = 'none';
		} 
    else {				
		document.getElementById('ifUrl').style.display = 'none';
        document.getElementById('ifModule').style.display = 'none';			
        document.getElementById('ifPage').style.display = 'none';			
		document.getElementById('ifUri').style.display = 'none';
		} 
		
});
</script>

  <?php $lang_count=0;
                    foreach($lang as $language)
                    {?>        

                    <?php $lang_count++;   
                    } ?>  
          
             
              <!-- end -->
<div class="row"> 
    <div class="col-md-12">
        <div class="tabbable tabbable-custom">
            <!--                code  for Tab-->
            <ul class="nav nav-tabs">
             <?php $i=1;
                foreach($lang as $language)
                {?>        
                    <li id="<?php echo $i ?>" class="<?php echo($i==1)?'active':'';?>"><a href="#tab_1_<?php echo $i;?>" data-toggle="tab"><?php echo $language;?></a></li> 
                <?php $i++;   
                } ?>            
              <!-- end of Tab --> 
            </ul>
          <!--form start -->  
            <?php
                echo validation_errors('<p style="color: red;">', '</p>');
                echo form_open_multipart('admin/navigation/submit', 'class="form-horizontal row-border" id="validate-1"');	
            ?>
          <div class="tab-content">
              <?php $i=1;
              foreach($lang as $key=>$language){ ?>
              
              <!-- content div with tab -->
            <div class="tab-pane <?php echo($i==1)?'active':'';?>" id="tab_1_<?php echo $i;?>">
                    <?php echo form_hidden('language_'.$language,$key);?>
                    <div class="form-group"> 
                            <label class="col-md-2 control-label">Title </label> 
                        <div class="col-md-10"> 
                            <?php echo form_input('title_'.$language, ${'title_'.$language}, 'class="form-control "');?>
                        </div> 
                    </div> 
                        
                    <div class="form-group"> 
                            <label class="col-md-2 control-label">Parent </label> 
                        <div class="col-md-10"> 
                            <?php $selected = ${'parent_id_'.$language};$options = $parent;
                            echo form_dropdown('parent_id_'.$language, $options, $selected, 'class="form-control"');?>
                        </div> 
                    </div>  
                                
                    <div class="form-group"> 
                            <label class="col-md-2 control-label">Link Type</label> 
                    <div class="col-md-8" > 

 
              <?php ${'navtype_check_'.$language} = ${'navtype_'.$language};
                    if((isset(${'navtype_check_'.$language}))&& ${'navtype_check_'.$language} == 'Module'){
                       ${'result_module'.$language} = 'checked';${'result_page'.$language} = '';${'result_url'.$language} = '';${'result_uri'.$language} = ''; 
                    }  
                    else if((isset(${'navtype_check_'.$language}))&& ${'navtype_check_'.$language} == 'Page'){
                      ${'result_module'.$language} = '';${'result_page'.$language} = 'checked';${'result_url'.$language} = '';${'result_uri'.$language} = ''; 
                    }       
                    else if((isset(${'navtype_check_'.$language}))&& ${'navtype_check_'.$language} == 'URI'){
                       ${'result_module'.$language} = '';${'result_page'.$language} = '';${'result_url'.$language} = '';${'result_uri'.$language} = 'checked';  
                    }
                    else if((isset(${'navtype_check_'.$language}))&& ${'navtype_check_'.$language} == 'URL'){
                       ${'result_module'.$language} = '';${'result_page'.$language} = '';${'result_url'.$language} = 'checked';${'result_uri'.$language} = ''; 
                    }
                    else {
                       ${'result_module'.$language} = '';${'result_page'.$language} = '';${'result_url'.$language} = '';${'result_uri'.$language} = '';  
                    }
             ?>
 
                            <input type="radio" name="navtype_<?php echo $language; ?>" value="Module_<?php echo $language;?>" <?php echo ${'result_module'.$language};?> onclick="javascript:navtypeCheck(this.id);" id=<?php echo 'moduleCheck_'.$language;?> style="margin-right:30px;" class="required"> <label>Module</label><br>
                            <input type="radio" name="navtype_<?php echo $language; ?>" value="Page_<?php echo $language; ?>"<?php echo ${'result_page'.$language};?>  onclick="javascript:navtypeCheck(this.id);" id=<?php echo 'pageCheck_'.$language;?> style="margin-right:30px;"><label>Page</label><br>
                            <input type="radio" name="navtype_<?php echo $language; ?>" value="URI_<?php echo $language; ?>"<?php echo ${'result_uri'.$language};?> onclick="javascript:navtypeCheck(this.id);" id=<?php echo 'uriCheck_'.$language;?> style="margin-right:30px;"><label>Uri</label><br>
                            <input type="radio" name="navtype_<?php echo $language; ?>" value="URL_<?php echo $language; ?>"<?php echo ${'result_url'.$language};?> onclick="javascript:navtypeCheck(this.id);" id=<?php echo 'urlCheck_'.$language;?> style="margin-right:30px;"><label>Url</label><br>
                            <label for="navtype" class="has-error help-block" generated="true" style="display:none;"></label>
                            <label for="navtype" class="has-error help-block" generated="true" style="display:none;"></label>
</div> 
                    </div>  
             <script type="text/javascript">
                     
                    function navtypeCheck(id) {
                        var ids = id.split('_');
                            lang=ids[1];
                        if (document.getElementById('moduleCheck_'.concat(lang)).checked) {
                            document.getElementById('ifModule_'.concat(lang)).style.display = 'block';
                            document.getElementById('ifPage_'.concat(lang)).style.display = 'none';	
                            document.getElementById('ifUri_'.concat(lang)).style.display = 'none';
                            document.getElementById('ifUrl_'.concat(lang)).style.display = 'none';
                        }
                        
                        else if (document.getElementById('pageCheck_'.concat(lang)).checked) {
                            document.getElementById('ifModule_'.concat(lang)).style.display = 'none';
                            document.getElementById('ifPage_'.concat(lang)).style.display = 'block';	
                            document.getElementById('ifUri_'.concat(lang)).style.display = 'none';
                            document.getElementById('ifUrl_'.concat(lang)).style.display = 'none';
                        }
                       
                        else if (document.getElementById('uriCheck_'.concat(lang)).checked) {	
                            document.getElementById('ifUri_'.concat(lang)).style.display = 'block';
                            document.getElementById('ifPage_'.concat(lang)).style.display = 'none';
                            document.getElementById('ifModule_'.concat(lang)).style.display = 'none';
                            document.getElementById('ifPage_'.concat(lang)).style.display = 'none';	       
                            document.getElementById('ifUrl_'.concat(lang)).style.display = 'none';	
                            }
                          
                        
                        else {	
                            document.getElementById('ifUrl_'.concat(lang)).style.display = 'block';
                            document.getElementById('ifModule_'.concat(lang)).style.display = 'none';	
                            document.getElementById('ifPage_'.concat(lang)).style.display = 'none';	
                            document.getElementById('ifUri_'.concat(lang)).style.display = 'none';
                            }
                    
                    }
                    
                    </script>                    
                                
                    <div id='ifModule_<?php echo $language;?>' style="display:none;" class="form-group"> 
                            <label class="col-md-2 control-label">Module</label> 
                    	<div class="col-md-10"> 
                            <?php $selected = ${'module_id_'.$language};$options = $modulelist;//moduleslist array has moduleslists from tbl_moduleslists
                            echo form_dropdown('module_id_'.$language, $options, $selected, 'class="form-control"');?>
</div> 
                    </div>
                                
                    <div id="ifPage_<?php echo $language;?>" style="display:none;" class="form-group"> 
                            <label class="col-md-2 control-label">Page</label> 
                    	<div class="col-md-10"> 
                            <?php $selected = ${'page_id_'.$language};$options = $pagelist;//moduleslist array has moduleslists from tbl_moduleslists
                            echo form_dropdown('page_id_'.$language, $options, $selected, 'class="form-control"');?>
</div> 
                    </div>
                                
                    <div id="ifUri_<?php echo $language;?>" style="display:none;" class="form-group"> 
                            <label class="col-md-2 control-label">Site Link(URI)</label> 
                    	<div class="col-md-10"> 
                            <?php echo form_input('site_uri_'.$language, ${'site_uri_'.$language}, 'class="form-control"');?>
</div> 
                    </div>
                                
                    <div id="ifUrl_<?php echo $language;?>" style="display:none;" class="form-group"> 
                            <label class="col-md-2 control-label">Link URL</label> 
                    	<div class="col-md-10">                   
                            <?php empty($link_url) ? $link_url='http://' : $link_url ?>
                            <?php echo form_input('link_url_'.$language, $link_url, 'class="form-control"');?>
</div> 
                    </div> 
                         
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Status</label> 
                    	<div class="col-md-10"> 
                            <?php $selected = ${'status_'.$language};$options = array(
                              'draft'  => 'draft',
                              'live'    => 'live',
                            );                            
                            echo form_dropdown('status_'.$language, $options, $selected,'class="form-control"');?>
</div> 
                    </div>   
                    
                   
                        <?php // echo form_hidden(array('name'=>'language_'.$language,'value'=>$key,'class'=>'form-control ', 'id'=>'language_'.$language.''));?>
                        <?php echo form_hidden('update_id_'.$language,${'update_id_'.$language});?>
                   
                    
                <?php 
                    if($i==$lang_count){	
                        echo form_submit('submit','Save','class="btn btn-primary pull-right"'); //name,value...type is default submit 
                 }?>
                
            </div>
              <!-- end of content div -->
                <?php $i++;
                   
              } ?>
          </div>  
             
           <?php 
           $group_id = $this->uri->segment(4); echo form_hidden('group_id',$group_id);
           echo form_close(); ?>
          
          <!-- form close -->
        </div>
    </div>
</div>
