<div class="manage">
    <?php if (isset($permission['add'])) { ?>
        <input type="button" value="Add Content" id="create" onclick="location.href = '<?php echo base_url() ?>admin/pages/create';"/>  
    <?php } ?>

</div>
<div class="col-sm-2">
    <?php echo form_input('title', '', 'class="form-control searchKeys" data-type="varchar" id="title" placeholder="Title"'); ?>
</div>
<div class="col-sm-2">
    <?php echo form_input('description', '', 'class="form-control searchKeys" data-type="varchar" id="sub_title" placeholder="Description"'); ?>
</div>
<div class="col-sm-2">
    <?php echo form_button('submit', 'Search', 'class="btn btn-primary" id="search"'); ?>
</div>

<div class="widget box" id="replaceTable"> 
    <div><?php
        if (($this->session->flashdata('operation'))) {
            echo $this->session->flashdata('operation');
        }
        ?></div>
    <div class="widget-header"> 
        <h4><i class="icon-reorder"></i> Pages </h4> 

        <div class="toolbar no-padding"> 
            <div class="btn-group"> 
                <span class="btn btn-xs widget-collapse">
                    <i class="icon-angle-down"></i>
                </span> 
            </div> 
        </div> 
    </div>


    <div class="widget-content"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive tablesorter"id="tblData"> 
            <thead> 
                <tr> 
                    <th class="checkbox-column">S.No.</th> 
                    <?php foreach ($columns as $column) { ?>
                        <th><?php
                        $this->lang->load('pages','english');
                        echo $this->lang->line(implode(' ', explode('_', $column))); ?></th>
                    <?php } ?>
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 

            <tbody> 
                <?php $sno = ($page - 1) * $per_page + 1; ?>
                <?php foreach ($query->result() as $row) { ?>

                    <tr> 
                        <td class="checkbox-column"><?php
                            echo $sno;
                            $sno++;
                            ?></td> 
                        <?php foreach ($columns as $column) {
                            ?>
                                <?php if ($column == "attachment") { ?><td><img src="<?php echo base_url(); ?>uploads/pages/<?php echo $row->$column; ?>" style="height:50px;"/></td>
                                <?php } else { ?>
                                <td><?php echo ucfirst($row->$column); ?></td>
                                <?php
                            }
                        }
                        ?>

                        <td class="edit">
                            <?php if (isset($permission['view'])) { ?><a href="<?php echo base_url() ?>admin/pages/view/<?php echo base64_encode($row->id); ?>"><i class="icon-eye-open"></i></a><?php } ?>
                            <?php if (isset($permission['edit'])) { ?>&nbsp;/&nbsp; 
                                <a href="<?php echo base_url() ?>admin/pages/create/<?php echo base64_encode($row->id); ?>"><i class="icon-pencil"></i></a> <?php } ?>                  

                            <?php if (isset($permission['delete'])) { ?>&nbsp;/&nbsp; <a href="<?php echo base_url() ?>admin/pages/delete/<?php echo base64_encode($row->id); ?>" onclick="return confirm('Are you sure, you want to delete it?');"><i class="icon-trash"></i></a><?php } ?>
                        </td> 
                    </tr> 

                <?php } ?>                
            </tbody> 
        </table> 
        <div id="pagination">
            <div class="col-sm-2">
                <input type='text' id="goto" class='form-control' placeholder="Goto Page Number"/>
            </div>
            <div class="col-sm-6" style="margin-left: 30px;">
                <span class='total' a="<?php echo $total_page; ?>" b="<?php echo $theUrl; ?>">Showing Page <b><?php echo $page; ?></b> of <b><?php echo $total_page; ?></b></span>
            </div>
            <ul class="pager">
                <?php
                if (!empty($page_links)) {
                    echo $page_links;
                }
                ?>

            </ul>
        </div>
    </div>

</div>

<!--end of class="widget box"-->
