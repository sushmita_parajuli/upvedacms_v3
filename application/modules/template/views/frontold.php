<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

<!--<title><?php echo $site_settings['site_name']; ?></title>
<meta name="title" content="<?php echo $site_settings['site_name']; ?>">
<meta name="keywords" content="<?php echo $site_settings['meta_topic']; ?>">
<meta name="description" content="<?php echo $site_settings['meta_data']; ?>">-->


        <?php
        if (isset($title)) {
            ?>
            <title><?php echo $title; ?></title>
            <meta name="title" content="<?php echo $title; ?>">
            <meta name="keywords" content="<?php echo $search_keys; ?>">
            <meta name="meta_description" content="<?php echo $meta_description; ?>">
<?php
} elseif (isset($seo[0])) {
    ?>
            <title><?php echo ucfirst($this->uri->segment(1));?></title>
            <meta name="title" content="<?php echo $seo[0]['title']; ?>">
            <meta name="keywords" content="<?php echo $seo[0]['search_keys']; ?>">
            <meta name="meta_description" content="<?php echo $seo[0]['meta_description']; ?>">

<?php
} else {
    ?>

            <title> <?php echo 'Contact Us' ?></title>
            <meta name="title" content="<?php echo 'Contact Us' ?>">
            <meta name="keywords" content="<?php echo 'Contact Us , Nagatour, Tour In Nepal' ?>">
            <meta name="meta_description" content="<?php echo 'Contact Us , Nagatour, Tour In Nepal' ?>">
<?php } ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>uploads/settings/<?php echo $site_settings['favicon']; ?>">

        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/responsive-slider.css">
         <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/bootstrap.min.css">
        <!-- Bootstrap -->

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>design/frontend/css/carouselengine/initcarousel-1.css">
<!--        <script src="js/jquery-1.11.3.min.js"></script>-->
        <!--social icon-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/bootstrap-social.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/bootstrap.min.css">
        <!--<link rel="stylesheet" href="css/bootstrap-social.less">-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/admin/css/fontawesome/font-awesome.min.css"> 
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/font-awesome.css">
        
         <script src="<?php echo base_url(); ?>design/frontend/js/jquery-1.9.1.min.js"></script>
        <!--social icon end-->

        <script src="<?php echo base_url(); ?>design/frontend/js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>design/frontend/js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="<?php echo base_url(); ?>design/frontend/js/bootstrap.min.js"></script>
    </head>
    <body>
        <!--<div class="container">-->    
        <!--navigation bar------------------------------------------------------------>
        <div class="col-lg-12 top_bar">
            <div class="col-lg-2"> 
<!--                <h1 style="color:#8B171A;">Anubhuti</h1>-->
                <a title="Anubhuti Nepal" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>uploads/settings/<?php echo $site_settings['logo']; ?>" alt="Anubhuti" style="text-align:center; height:125px;"></a>
<!--                width: 240px; height: 108px;-->
            </div>
            <div class="col-lg-10">
                        <div class="navbar navbar-default hidden-lg" role="navigation">
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                </div>
                        </div>

                <div class="navbar-collapse collapse pull-right" style="height: 1px;">
                    <ul class="nav navbar-nav">

<?php foreach ($headernav as $header) {//still need to work on slug ?> 
    <?php if (empty($header['children'])) {
        echo '<li>'; /* this is for no-child */
    } else { ?>                    	
                                <li class="dropdown">
                            <?php }/* this is for dropdown */ ?>

                                <?php if (!empty($header['children'])) {/* this is for dropdown */ ?> 
                                    <?php if ($header['navtype'] == 'URL') {
                                        $prefix = '';
                                    } else {
                                        $prefix = base_url();
                                    } ?>
                                    <a href="<?php echo $prefix . $header['href'] ?>" target="<?php echo $header['target'] ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $header['title'] ?>                                    <b class="caret"></b>                                
                                    <?php }/* end of if */ else { /* this is for no-child */
                                        ?>                            
                                            <?php if ($header['navtype'] == 'URL') {
                                                $prefix = '';
                                            } else {
                                                $prefix = base_url();
                                            } ?>
                                        <a href="<?php echo $prefix . $header['href'] ?>" target="<?php echo $header['target'] ?>"><?php echo $header['title'] ?>
                                            <?php } ?>                           
                                    </a>

                                        <?php if (!empty($header['children'])) {/* this is for dropdown */ ?> 
                                        <ul class="dropdown-menu">
                                        <?php foreach ($header['children'] as $child) { ?>  
                                                <li>                                            
            <?php if ($child['navtype'] == 'URL') {
                $prefix = '';
            } else {
                $prefix = base_url();
            } ?>
                                                    <a href="<?php echo $prefix . $child['href']; ?>" target="<?php echo $child['target'] ?>"><?php echo $child['title']; ?></a>
                                                </li>
        <?php }/* end of child foreach */ ?>
                                        </ul>
            <?php } ?>  

                            </li>
        <?php }/* end of parent foreach */ ?>

                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
        <div class="clearfix"></div><!--banner-------------------------------------------------------------------->

        <?php
        $first_bit = $this->uri->segment(1);
        if ($first_bit == "" || $first_bit == "home") {
            ?>

            <!--								
                                            ============================================start of responsive slider============================
                                                    
                                                            <div class="responsive-slider" data-spy="responsive-slider" data-autoplay="true">
                                                    <div class="slides" data-group="slides">
                                                            <ul>
    <?php foreach ($banner as $row) { //displaying all banner  ?> 
                                                                                <li>
                                                                                  <div class="slide-body" data-group="slide">
                                                                                        <img src="<?php echo base_url(); ?>uploads/banner/<?php echo $row['attachment'] ?>" style="width:1140px;">
                                                                                  </div>
                                                                                </li>
    <?php } ?>
                                                            </ul>
                                                    </div>
                                                    <a class="slider-control left" href="#" data-jump="prev"><</a>
                                                    <a class="slider-control right" href="#" data-jump="next">></a>
                                                    
                                       </div>
                                      
                                            <script src="<?php echo base_url(); ?>design/public/js/responsive-slider.js"></script>
                                    <script src="<?php echo base_url(); ?>design/public/js/jquery.event.move.js"></script>
                                     
                                      ==============================================end of responsive slider============================   
            -->





            <div class="bckgrd-pic">
                <div class="col-lg-offset-2 col-lg-8">
                    <div id="slider" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <!-- <ol class="carousel-indicators">
                          <li data-target="#slider" data-slide-to="0" class="active"></li>
                          <li data-target="#slider" data-slide-to="1"></li>
                          <li data-target="#slider" data-slide-to="2"></li>
                        </ol> -->

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <?php 
                            $active='active';
                            foreach ($banner as $row) { //displaying all banner  ?> 
                          
                            <div class="item <?php echo $active?>">
                                <div class="img-heading"><h2><?php echo $row['title']?></h2></div> 
                                <div class="img-description"><p><?php echo $row['sub_title']?></p></div>
                                <center> <img class="img-responsive pic" src="<?php echo base_url(); ?>uploads/banner/<?php echo $row['attachment']?>" alt="lighthouse" style="width: 100%;"></center>
                            </div>
                                <?php
                                $active='';

                                }

                                ?>

                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control bckgrd-none" href="#slider" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" style="top:60% !important;"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control bckgrd-none" href="#slider" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true" style="top:60% !important;"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        
                            <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
                    </div>
                </div>
            </div>
        
            <div class="clearfix"></div>
            <!-- end of slider -->
            <!-- about start -->
            <div class="backgrd-abt" align="center">
                <!--<div class="col-lg-6"></div>-->
                <!--<div class="col-lg-6">-->
                    <div class="Abt-us">
                    <h1 class="text-uppercase" style="color:#8B191B;">About us</h1>
                    <div class="col-lg-offset-1 col-lg-10 " style="padding-bottom: 20px;">
                         <?php foreach($introduction as $row){?>                                         
<!--                                      <h4><?php echo ucwords($row['title']);?></h4><br>-->
                                 <p style=""><?php echo word_limiter($row['description'],74);?></p>                                       
                        <?php }?>
                                 <a href="<?php echo base_url()?><?php echo $row['slug'];?>"><button type="button" class="btn btn-primary class_button" > READ MORE >> </button></a>
<!--                        <p style="">Anubhuti Nepal is a nonprofit organization registered with the purpose of providing support for promotion and protection of the Rights of the Foreign Employed Nepalese (FEN) and their families/children; and providing rescue/relief and rehabilitation services in case of emergencies (e.g., illness, accidents, reparation and deaths). Anubhuti Nepal’s target beneficiaries are all Nepali Youths (women and men) who are planning to go abroad for employment, are living abroad for employment or who have worked abroad.</p>
                        <button type="button" class="btn btn-primary class_button">Read More >></button>-->
                    </div>
                    <!--</div>-->
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- end of about -->

            <!-- our stories/our approach -->
            <div class="our-stories" style="margin:0px auto;">
                <center><h2 style="color:rgba(177, 0, 0, 0.75);">Our Stories/Our Approach</h2></center>
                <!-- Insert to your webpage where you want to display the carousel -->
                <div id="amazingcarousel-container-1">
                    <div id="amazingcarousel-1" style="display:block;position:relative;width:100%;margin:0px auto 0px;">
                        <div class="amazingcarousel-list-container">
                            <ul class="amazingcarousel-list">
                                 <?php foreach ($stories as $row){?>
                                <li class="amazingcarousel-item">
                                    <div class="amazingcarousel-item-container">
                                        <div class="amazingcarousel-image"><a href="<?php echo base_url();?>stories/detail/<?php echo $row ['slug']?>"><img src="<?php echo base_url(); ?>uploads/stories/<?php echo $row['attachment'];?>"  alt="" /></a></div>
                                        <!--<div class="amazingcarousel-image"><a href="<?php echo base_url(); ?>uploads/stories<?php echo $row['attachment'];?>"   class="html5lightbox" data-group="amazingcarousel-1"><img src="<?php echo base_url(); ?>uploads/stories/<?php echo $row['attachment'];?>"  alt="" /></a></div>-->
                                        <div class="amazingcarousel-text">
                                            <cite> <a href="<?php echo base_url();?>stories/detail/<?php echo $row ['slug'];?> "><?php echo $row['title'] ?></a></cite>
                                            <blockquote><?php echo word_limiter($row['description'],20)?></blockquote>
                                        </div>
                                        <div style="clear:both;"></div>                    </div>
                                </li>
                                <?php }?>
                            </ul>
                            <div class="amazingcarousel-prev"></div>
                            <div class="amazingcarousel-next"></div>
                        </div>
                        <div class="amazingcarousel-nav"></div>
<!--                        <div class="amazingcarousel-engine"><a href="http://amazingcarousel.com">jQuery Scroller</a></div>-->
                    </div>
                </div>
                <script src="<?php echo base_url(); ?>design/frontend/css/carouselengine/amazingcarousel.js"></script>
                <script src="<?php echo base_url(); ?>design/frontend/css/carouselengine/initcarousel-1.js"></script>
            </div>
            <div class="donate container">
                <center><button type="button" class="btn btn-danger btn-donate class_button" style="padding: 16px 40px !important;">Donate</button></center>

            </div>

            <div class="clearfix"></div>
            
                
                
                
            <?php } else { ?>
            
            
            <!--         start of the content ------------------------------------------>
            <?php if(isset ($navigation))
            {?>
            
            <div id="content">
                <div class="container other-abt">
                    <div class="col-md-3">
                        <div class="abt-head">
                        <h1 style="margin-top: 3px; margin-bottom: 2px;">
                        <!-- color: rgba(177, 0, 0, 0.75); --> 
                                <?php
                                if ($this->uri->segment(3)) {
                                    $title_new = $this->uri->segment(3);
                                    $title = urldecode($title_new);
                                } else {
                                    $title = $this->uri->segment(1);
                                }
                                if (strpos($title, '_') != false) {
                                    $title_array = explode('_', $title);
                                } else {
                                    $title_array = explode('-', $title);
                                }

                                foreach ($title_array as $tarray) {
                                    echo ucwords(strtolower($tarray));
                                    echo ' ';
                                }
                                //   echo $title_echo;
                                ?>
                            </h1>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <?php
                        if (!isset($view_file)) {
                            $view_file = "";
                        }
                        if (!isset($module)) {
                            $module = $this->uri->segment(1);
                        }
                        if (($view_file != '') && ($module != '')) {
                            $path = $module . "/" . $view_file;
                            $this->load->view($path);
                        } else {
                            $new_description = str_replace("../../../", "./", $description); //replacing image location as root location to display image
                            $description=nl2br($new_description); //put your designs here
                            $count= strlen($description);
                            if($count>1000)
                            {?>
                              <div class="p_greater_than">  
                                  <?php echo $description;?>
                              </div>
                    
                           <?php }
                           else{?>
                               <div class="p_less_than">  
                                  <?php echo $description;?>
                              </div>
                          <?php }
                        }

                        //echo '<pre>';
                        //print_r($this->session);
                        //die();
                        ?>
                    </div>
                    
                    
                  
                </div>
            </div>
            <?php }else {?>
             
            <div id="other_content">
                <div class="container other-abt-div">
                    <div class="col-md-12">
                        <div>
                            <div class="col-md-3 abt-head">
                            <h1 style="margin-top: 3px; margin-bottom: 2px;">
                            <!-- color: rgba(177, 0, 0, 0.75); --> 
                                    <?php
                                    if ($this->uri->segment(3)) {
                                        $title_new = $this->uri->segment(3);
                                        $title = urldecode($title_new);
                                    } else {
                                        $title = $this->uri->segment(1);
                                    }
                                    if (strpos($title, '_') != false) {
                                        $title_array = explode('_', $title);
                                    } else {
                                        $title_array = explode('-', $title);
                                    }

                                    foreach ($title_array as $tarray) {
                                        echo ucwords(strtolower($tarray));
                                        echo ' ';
                                    }
                                    //   echo $title_echo;
                                    ?>
                                </h1>
                            </div>
                            <div class="col-md-9"></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                        <?php
				if(!isset($view_file)){
					$view_file="";
				}
				if(!isset($module)){
					$module = $this->uri->segment(1);
				}
				if(($view_file!='') && ($module!='')){
					$path = $module."/".$view_file;
					$this->load->view($path);
				} 
				else {
					$new_description = str_replace("../../../","./",$description); //replacing image location as root location to display image
					echo nl2br($new_description);//put your designs here
				}
				
				//echo '<pre>';
				//print_r($this->session);
				//die();
                            ?>
                        </div>
                    </div>
                    
                    
                  
                </div>
            </div>
            <?php }?>
            
<?php } ?>
        <div class="zigzag"></div>
        <div class="footer">
            <div class="container" style="background: transparent;">
                <div class="col-md-3 col-sm-6">
                    <div class="services">
                        <h4>About Us</h4>
                        <hr class="hr_small_width">
                        <?php foreach ($headernav[1]['children'] as $footer) {//still need to work on slug ?>
                            <p>                                            
                               <a href="<?php echo $footer['href']; ?>" target="<?php echo $footer['target'] ?>"><?php echo $footer['title']; ?></a>
                            </p>
                        <?php } ?>

                    </div>   
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="contact_address">
                        <h4>Contact Address </h4>
                        <hr class="hr_small_width">
                        <p><i class="icon-map-marker"></i>&nbsp;&nbsp;Kupondole, Lalitpur-1 </p>
                        <p><i class="icon-phone"></i>&nbsp;&nbsp;+977-1-5521651</p>
                        <p><i class="icon-phone"></i>&nbsp;&nbsp;+977-9851045579</p>
                        <p><i class="icon-envelope"></i>&nbsp;&nbsp;anunhutinepal@gmail.com</p>
                        <p><i class="icon-envelope"></i>&nbsp;&nbsp;trdahal@gmail.com</p>
                    </div>  
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="connect_with_us">
                        <h4>Connect With Us</h4>
                        <hr class="hr_small_width" style="margin-bottom:10px;">
                        <div class="icon-social social">
                            <ul class="icons">
                                <li><a href="#"><i class="fa fa-lg fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-lg fa-twitter"></i></a></li>
                                <!--<li><a href="#"><i class="fa fa-lg fa-google-plus"></i></a></li>-->
                                <li><a href="#"><i class="fa fa-lg fa-youtube"></i></a></li>
                            </ul>
                        </div>                                
                    </div>   
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="quick_contact">
                        <h4>Quick Contact</h4>
                        <hr class="hr_small_width">
<?php
echo form_open_multipart('', 'id="validate-1"');
?>
                        <div class="form-group" style="margin-bottom:20px">
                            <input type="email" class="form-control" id="email" placeholder="Email" >
                            <div style="color:#FF0000; position: absolute;" id="errmsgemail"></div>
                        </div>

                        <div class="form-group" style="margin-bottom:20px">
                        <textarea rows="4" cols="36" type="message" class="form-control" id="message" placeholder="Message"></textarea>
                            

                        </div>
                        <button type="button" id="send" class="btn btn-default button_border_radius">SEND</button>
                        <div id="success" style="display: none;">
                            <p style="color:#29a54a !important;">Subscribed Successfully</p>
                        </div>
                        <div id="failure" style="display: none;">
                            <p style="color:#ef2121;">Error during subscription</p>
                        </div>
<?php echo form_close() ?> 
                    </div>    
                </div>
            </div>
        </div><!---end-content--->
        <div class="footer_copyright">
            <div class="container" style="background: transparent;">
                <p style="font-size: 13px; padding-top: 10px;"> &copy; <?php echo date("Y"); ?> All rights reserved! by Anubhuti Pvt. Ltd.</p>
            </div>
        </div>

        <!--          <div class="footer">
                    <div class="">
                      <div class="col-lg-8">
                          <div class="footernav">
                            <ul class="nav">
<?php foreach ($footernav as $footer) {//still need to work on slug ?> 
                                            <li>
            <?php if ($footer['navtype'] == 'URL') {
                $prefix = '';
            } else {
                $prefix = base_url();
            } ?>
                                            <a href="<?php echo $prefix . $footer['href']; ?>" target="<?php echo $footer['target']; ?>"><?php echo $footer['title']; ?></a>
                                        </li>
<?php } ?>
                            </ul>
                          </div>
                      </div>
                      <div class="col-lg-4">
                        <p>Developed By <a href="http://upvedatech.com"><img src="<?php echo base_url(); ?>design/frontend/img/logo.png" width="150" height="70"></a></p>
                      </div>
                    </div>
                    <p>All rights reserved! &copy; <?php echo date("Y"); ?></p>
                  </div>-->
    </div><!---end-content--->

    <!--</div>end of container-->
    <script src="<?php echo base_url(); ?>design/frontend/js/plugins.js"></script> 
    <script src="<?php echo base_url(); ?>design/frontend/js/main.js"></script> 
    <script src="<?php echo base_url(); ?>design/frontend/js/jquery.event.move.js"></script>
    <script src="<?php echo base_url(); ?>design/frontend/js/responsive-slider.js"></script>
        
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. --> 
    <script>
        (function (b, o, i, l, e, r) {
            b.GoogleAnalyticsObject = l;
            b[l] || (b[l] =
                    function () {
                        (b[l].q = b[l].q || []).push(arguments)
                    });
            b[l].l = +new Date;
            e = o.createElement(i);
            r = o.getElementsByTagName(i)[0];
            e.src = '//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e, r)
        }(window, document, 'script', 'ga'));
        ga('create', 'UA-XXXXX-X');
        ga('send', 'pageview');
    </script>
    <!-- start of code for clickable dropdown menu bar  -->
        <script>
            jQuery(function($) {
                $('.navbar .dropdown').hover(function() {
                    $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

                }, function() {
                    $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

                });

                $('.navbar .dropdown > a').click(function(){
                    location.href = this.href;
                });

            });
        </script>
                <script>
    $('#send').click(function ()
    {
        
    if($('#message').val() == '')
        {
           
            $('#message').css('border-color','#FF0000');
            $('#message').focus();
            return false;
        }
        
    if($('#email').val() == '')
        {
              
            $('#email').css('border-color','#FF0000');
            $('#email').focus();
            return false;
        }
        
    else{        

            subscribe();
        }
    });
    function subscribe()
    
    {
        
        var message = $("#message").val();
        var email = $("#email").val();
        $.ajax({
            type:'POST',
            data: 'message='+message + '&email=' + email,
            
            url: <?php base_url()?>'subscribe/subscribe_user',
            success:function(data)
            {
                if(data=='true'){
                     $("#message").val('');
                     $('#message').css('border-color','#cccccc');
                     $("#email").val('');
                     $("#failure").hide();
                     $("#success").show();
                }
                else{
                     $("#failure").show();
                     $("#success").hide();                    
                }
            }
        });
    }
    
</script>


<script>
    $('#message').change(function()
{
    
   var name2 = $("#message").val();
  if(name2!='')
  {
      $('#message').css('border-color','#cccccc');
  }
  
});
  $('#message').blur(function()
    {
      var name2 = $("#message").val(); 
    if(name2=='')
  {
    $('#message').css('border-color','#FF0000');  
  } 
    });
    $('#email').change(function()
{
  checkEmail();  
});
function checkEmail()
{

              $('#email').filter(function()
              {
                    var emil=$('#email').val();
                    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                    if( !emailReg.test( emil ) ) 
                    {
//                        alert('plz enter correct email address');
//                        return false;
                        
                        $('#errmsgemail').html('Please Enter Valid Email');
                        $('#errmsgemail').show();
                        $('#email').css('border-color','#FF0000');

                       
                    } 
                    else 
                    {
                        $('#errmsgemail').fadeOut("slow");
                        $('#email').css('border-color','#cccccc');
                     }
                });
 };
</script>
        <!-- end of code for clickable dropdown menu bar  -->

</body>
</html>

