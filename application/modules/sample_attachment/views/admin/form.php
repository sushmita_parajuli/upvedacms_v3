
<script type="text/javascript" src="<?php echo base_url(); ?>assets/tinymce_4/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">

    tinymce.init({
        selector: "textarea",
        // ===========================================
        // INCLUDE THE PLUGIN
        // ===========================================

        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste jbimages",
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],
        // ===========================================
        // PUT PLUGIN'S BUTTON on the toolbar
        // ===========================================

        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages | image",
        toolbar3: "print preview media | forecolor backcolor emoticons",
        // ===========================================
        // SET RELATIVE_URLS to FALSE (This is required for images to display properly)
        // ===========================================
        image_advtab: true,
        relative_urls: false

    });

</script>
<!-- /TinyMCE -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'bootstrap/css/datepicker.css' ?>"/>
<link rel="stylesheet/less" type="text/css" href="<?php echo base_url() . 'bootstrap/less/datepicker.less' ?>" />

<script src="<?php echo base_url() . 'bootstrap/js/bootstrap-datepicker.js' ?>"></script>
<script src="<?php echo base_url() . 'bootstrap/datepicker/js/bootstrap-datepicker.js' ?>"></script>
<div class="row"> 
    <div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
                <h4><i class="icon-reorder"></i>Sample With Attachment</h4> 
            </div> 
            <div class="widget-content">
                <?php
                echo validation_errors('<p style="color: red;">', '</p>');
                echo form_open_multipart('admin/sample_attachment/submit', 'class="form-horizontal row-border" id="validate-1"');
                ?>                
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Title<span class="required">*</span></label> 
                    <div class="col-md-10"> 
                        <?php echo form_input('title', $title, 'class="form-control required"'); ?>
                    </div>
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Description</label> 
                    <div class="col-md-10">
                        <?php echo form_textarea(array('id' => 'elm1', 'name' => 'description', 'value' => $description, 'rows' => '15', 'cols' => '80', 'style' => 'width: 100%', 'class' => 'form-control')); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Meta Description</label> 
                    <div class="col-md-10"> 
                        <?php echo form_input('meta_description', $meta_description, 'class="form-control required"'); ?>
                    </div>
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Search Keys</label> 
                    <div class="col-md-10"> 
                        <?php echo form_input('search_keys', $search_keys, 'class="form-control required"'); ?>
                    </div>
                </div>

                <div class="form-group" id='attachmentimage'>
                    <label class="col-md-2 control-label">Attachments <span class="required">*</span></label> 
                    <div class="col-md-2"> 
                        <?php
                        if (!empty($update_id)) {

                            $attach_prop = array(
                                'type' => 'file',
                                'name' => 'userfile[]',
                                'value' => $attachment,
                                'multiple'=>'',
                            );
                        } else {
                            $attach_prop = array(
                                'type' => 'file',
                                'name' => 'userfile[]',
                                'value' => $attachment,
                                'multiple'=>'',
                            );
                        }
                        ?>

                        <?php echo form_upload($attach_prop); ?>
                        <p class="help-block">
                            Images only (jpg/jpeg/gif/png)</p>
                        <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                            <?php if (!empty($update_id)) { ?>
                                <img src="<?php echo base_url(); ?>uploads/sample_attachment/<?php echo $attachment; ?>" style="height:100px;"/>
                            <?php } ?>
                        </label>
                    </div>

                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Status</label>
                    <div class="col-md-10"> 
                        <?php
                        $selected = $status;
                        $options = array(
                            'draft' => 'draft',
                            'live' => 'live',
                        );
                        echo form_dropdown('status', $options, $selected, 'class="form-control"');
                        ?>
                    </div> 
                </div>

                <div class="form-actions"> 
                    <?php
                    echo form_submit('submit', 'Submit', 'class="btn btn-primary pull-right"'); //name,value...type is default submit 
                    if (!empty($update_id)) {
                        echo form_hidden('update_id', $update_id);
                    }
                    ?>
                </div>                 

                <?php echo form_close(); ?>      
                <script>
    $('#datepicker').datepicker();
    $('#datepicker2').datepicker();
                </script>
            </div> 
        </div> 
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url() . "bootstrap/datetimepicker/jquery/jquery-1.8.3.min.js" ?>" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url() . "bootstrap/js/bootstrap.min.js" ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . "bootstrap/datetimepicker/js/bootstrap-datetimepicker.js" ?>" charset="UTF-8"></script>

<script type="text/javascript">
    $('.form_datetime').datetimepicker({
        weekStart: 0,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1
    });
</script>  