<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_moduleslist extends Mdl_crud {

    protected $_table = "up_modules";
    protected $_primary_key = 'id';

    function __construct() {
        parent::__construct();
    }

    function _delete($id) {
        $table = $this->_table;
        $this->db->where($this->_primary_key, $id);
        $this->db->delete($table);
        $this->delete_permissions_of_that_module_too($id);
    }

    function delete_permissions_of_that_module_too($id) {
        $table = 'up_permissions';
        $query = $this->db->get($table)->result();
        foreach ($query as $row) {
            $roles_array = unserialize($row->roles);
            foreach ($roles_array as $check) {
                if ($check == $id) {
                    unset($roles_array[$id]);
                    $new_role = serialize($roles_array);
                    $this->db->where($this->_primary_key, $row->id);
                    $this->db->update($table, array('roles' => $new_role));
                }
            }
        }
    }

    function get_modules_dropdown() {
        $this->db->select('id, title');
        $this->db->order_by('title');
        $dropdowns = $this->db->get('up_modules')->result();
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->title;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

    function get_module_ids() {
        $this->db->select($this->_primary_key);
        $this->db->order_by($this->_primary_key);
        $dropdowns = $this->db->get('up_modules')->result();
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->id;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

    function get_id_from_modulename($modulename) {
        $table = $this->_table;
        $this->db->select($this->_primary_key);
        $this->db->where('slug', $modulename);
        $query = $this->db->get($table)->result();
        $result = $query[0]->id;
        return $result;
    }

    function get_all_slug_from_module() {
        $table = $this->_table;
        $this->db->select('slug');
        $query = $this->db->get($table)->result();
        $i = 1;
        foreach ($query as $row) {
            $new_array[$i] = $row->slug;
            $i++;
        }
        return $new_array;
    }

    function get_slug_from_moduleid($module_id) {
        $table = $this->_table;
        $this->db->select('slug');
        $this->db->where($this->_primary_key, $module_id);
        $query = $this->db->get($table)->result();
        $result = $query[0]->slug;
        return $result;
    }

}
