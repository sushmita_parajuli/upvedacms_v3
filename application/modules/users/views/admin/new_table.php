<div class="widget box" id="replaceTable"> 
    <div><?php
        if (($this->session->flashdata('operation'))) {
            echo $this->session->flashdata('operation');
        }
        ?></div>

    <div class="widget-header"> 
        <h4><i class="icon-reorder"></i> Users </h4> 
        <div class="toolbar no-padding"> 
            <div class="btn-group"> 
                <span class="btn btn-xs widget-collapse">
                    <i class="icon-angle-down"></i>
                </span> 
            </div> 
        </div> 
    </div>

    <div class="widget-content"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive"> 
            <!--            <div class="col-sm-2" style="float: right;margin-bottom: 10px;">
                            <input type="text" class="form-control" id="search" placeholder="search"/>
                        </div>-->
            <thead> 
                <tr> 
                    <th class="checkbox-column">S.No.</th> 
                    <?php foreach ($columns as $column) { ?>
                        <th><?php echo ucfirst(implode(' ', explode('_', $column))); ?></th>
                    <?php } ?>
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 
            <tbody> 
                <?php $sno = ($page - 1) * $per_page + 1; ?>
                <?php foreach ($query->result() as $row) { ?>

                    <tr> 
                        <td class="checkbox-column"><?php
                            echo $sno;
                            $sno++;
                            ?></td> 
                        <?php
                        foreach ($columns as $column) {
                            if ($column == 'created_on') {
                                ?>
                                <td><?php echo date('h:i:s D, d M Y', strtotime($row->$column)); ?></td>
                            <?php } else { ?>
                                <td><?php echo $row->$column; ?></td>
                                <?php
                            }
                        }
                        ?>

                        <td class="edit">
                            <?php if (isset($permission['edit'])) { ?>
                                <a href="<?php echo base_url() ?>admin/users/create/<?php echo $row->id; ?>"><i class="icon-pencil"></i></a>
                            <?php }if (isset($permission['delete'])) { ?>
                                &nbsp;&nbsp;/&nbsp;&nbsp; 
                                <a href="<?php echo base_url() ?>admin/users/delete/<?php echo $row->id; ?>" onclick="return confirm('Are you sure, you want to delete it?');"><i class="icon-trash"></i></a>
                            <?php } ?>
                        </td> 
                    </tr> 

                <?php } ?>                
            </tbody> 
        </table> 
    </div>
    <div id="pagination">
        <div class="col-sm-2">
            <input type='text' id="goto" class='form-control' placeholder="Goto Page Number" style='margin-left:10px;margin-top: -10px;'/>
        </div>
        <div class="col-sm-6">
            <span class='total' a="<?php echo $total_page; ?>" b="<?php echo $theUrl; ?>">Showing Page <b><?php echo $page; ?></b> of <b><?php echo $total_page; ?></b></span>
        </div>
        <ul class="pager">
            <?php
            if (!empty($page_links)) {
                echo $page_links;
            }
            ?>
        </ul>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/searchPagination.js"></script>