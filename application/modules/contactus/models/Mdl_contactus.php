<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mdl_contactus extends CI_Model {
	
	function __construct() {
	parent::__construct();
	}
	
	function get_table() {
	$table = "up_contactus";
	return $table;
	}
	
	
	function get($order_by){
	$table = $this->get_table();	
	$this->db->select('up_contactus.*');
	$this->db->order_by($order_by);
	$query=$this->db->get($table);
	return $query;
	}
	
	
	function get_username_from_user_id($user_id){
	
	$this->db->where('id', $user_id);
	$query = $this->db->get($this->get_table());
	$result = $query->result();
	return $result[0]->display_name;
	
	}
	
	function get_position_dropdown()
	{		
		$this->db->select('id, name');
		//$this->db->order_by('id','DESC');
	    $dropdowns = $this->db->get('up_contactus')->result();
		
		
		foreach ($dropdowns as $dropdown)
		{
			$dropdownlist[$dropdown->id] = $dropdown->name;
		}
		if(empty($dropdownlist)){return NULL;}
		$finaldropdown = $dropdownlist;
		return $finaldropdown;
		return  $dropdowns;
	}
	
	
	function get_where($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$query=$this->db->get($table);
	return $query;
	}
	
	function _insert($data){
			
	$table = $this->get_table();
	$this->db->insert($table, $data);
	}
	
	function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}
	
	function _delete($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->delete($table);
	}
	
	function chpwd_update($id, $chpwd){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $chpwd);
	}
	
}